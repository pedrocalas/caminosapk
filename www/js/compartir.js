var app = {	

	/**
	 * Funciones para la aplicación de Amigos cercanos
	 * Utiliza las capas de caminosseguros y vecinos
	 * 
	 * @class
	 * @public
	 * @alias caminosseguros.amigos
	*/
		
    map:				null,		//Mapa
    puntoLocalizado: 	null, 		//Punto geolocalizado
    marcador:			null,		//Estilo de marcador para el vecino cercano
    idSeguimiento:		null,		//ID de seguimiento de position.watchPosition

 	
	/**
	 * Inicio de la aplicación. 
	 * Inicializa FastClick y los vecinos cercanos
	 * Crea el estilo para el marcador de amigo cercano
	 * @public
	 */
    inicioApp: function(){
 		FastClick.attach(document.body);	//Iniciamos FastClick
 		app.vecinosInicial 	= jQuery.extend(true, {}, vecinosfin2);
 		
 	    app.marcador =  L.AwesomeMarkers.icon({
 	    	icon: "coffee",
 	    	prefix:"fa",
 	    	markerColor: "red"
 	    });
    },
    
	/**
	 * Manejador del evento de dispositivo listo
	 * Crea el mapa y define los manejadores de botones
	 * @public
	 */
    dispositivoListo: function(){
    	app.crearMapa();
    	
    	//Manejadores de botones
        $("#compartirBoton").click(app.onBtnCompartir);
    },
    
    
	/**
	 * Creación del mapa, maneja sus eventos y añade las capas
	 * @public
	 */
    crearMapa: function(){
    	app.map = L.map("comparteMapa",{
	    	fadeAnimation: 			true,
	        zoomAnimation: 			false,
	        markerZoomAnimation: 	false
	    }).fitWorld();

      L.control.scale({
    	  imperial: false
      }).addTo(app.map);

      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicGVkcm9jYWxhcyIsImEiOiJjajJ4cjluMTMwMThtMndxOGg4Y3NoZWkzIn0.83VnuZwLtidmCHxuP1briA', {
      	maxZoom: 18,
		attribution: '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				     'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      	id: 'mapbox.streets'
      }).addTo(app.map);
    	
    	//Geolocalizamos
		app.seguimientoGelocalizacion();
		
		
		var biciStyle = {
	        	color: "#30B13B",
	        	weight: 4,
	        	opacity: 0.7,
	        	fillColor: "black",
	        	lineJoin: "round"
	        };

	    var layerCarrilBici = L.geoJson(bicicletafinjs,{style: biciStyle});
	    layerCarrilBici.addTo(map);
    	
    },
    
	/**
	 * Realiza el seguimiento de la geolocalización
	 * @public
	 */
    seguimientoGelocalizacion: function(){
    	app.idSeguimiento = navigator.geolocation.watchPosition(app.onLocationFound, app.onLocationError, {
    		timeout: 3000,
    		enableHighAccuracy: true
    	});
	},

	
	/**
	 * Manejador del evento Geolocalización encontrada
	 * @param {evt} Object Evento
	 * @public
	 */
    onLocationFound: function(evt){
    	var marcadorGeo = L.AwesomeMarkers.icon({
	    	icon: 'child',
	    	prefix:'fa',
	    	markerColor: 'blue'
	    });
    	app.puntoLocalizado = [evt.coords.latitude, evt.coords.longitude];
    	//var radius = evt.coords.accuracy / 2;
    	
        if(!app.marcadorGeolicalizado){
        	app.marcadorGeolicalizado = L.marker([evt.coords.latitude, evt.coords.longitude], {icon:marcadorGeo}).addTo(app.map);
        } else {
        	app.marcadorGeolicalizado.setLatLng(app.puntoLocalizado);
        	//app.marcadorGeolicalizado.setRadius(radius);
        }
        
        app.map.setView(app.puntoLocalizado, 14.5);
    },

    
	/**
	 * Manejador del evento Geolocalización errónea
	 * @param {error} Object Datos del error
	 * @public
	 */
    onLocationError: function(error){
    	alert(error.message);
    	console.log(error.code + ': ' + error.message);
    },
    
    
	/**
	 * Manejador del botón Geolocalizar
	 * @param {evt} Object Evento
	 * @public
	 */
    onBtnGeolocalizar: function(evt){
    	app.geolocalizar();
    },
    
    
	/**
	 * Crea una capa GeoJson y la añade al mapa
	 * @param {Object} datosCapa Datos de la capa
	 * @public
	 */
    anyadirCapaAMapa: function(datosCapa){
        var layer = L.geoJson(datosCapa,{});
        layer.addTo(app.map);
    },
    
    
	/**
	 * Manejador del botón Compartir
	 * @param {evt} Object Evento
	 * @public
	 */
    onBtnCompartir: function(evt){
    	alert("Botón Compartir pulsado");
    }
    
    
 };

$("#button-layer").click(function(){
	$("#controlCapas").toggle();
});
 
 
/////////////////////// 
//////INICIO
/////////////////////// 
if("addEventListener" in document){
	document.addEventListener('DOMContentLoaded', function(){
		app.inicioApp();
		//app.dispositivoListo();
	});

	document.addEventListener('deviceready', function(){
		app.dispositivoListo();
	});
}
