// $(document).ready(function(){
// //VISTA
//   var view = new ol.View({
//     center: ol.proj.transform([-60.6792, -31.6200], 'EPSG:4326', 'EPSG:3857'),
//     zoom: 16,
//     maxZoom: 25,
//     minZoom: 11
//   });
// //CAPA BASE MAPBOX
//   var mapbox = new ol.layer.Tile({
//     source: new ol.source.XYZ({
//         url: 'https://api.mapbox.com/styles/v1/pedrocalas/cj9xf83rz6muv2sobidhajg36/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicGVkcm9jYWxhcyIsImEiOiJjajJ4cjluMTMwMThtMndxOGg4Y3NoZWkzIn0.83VnuZwLtidmCHxuP1briA'
//     }),
//     visible: true,
//     name: 'mapbox'
//   });
//
//
//   var map = new ol.Map({
//     target: 'mapa',
//     controls: ol.control.defaults().extend([
//         new ol.control.ScaleLine(),
//         new ol.control.ZoomSlider()
//     ]),
//     renderer: 'canvas',
//     layers: [mapbox],
//     view: view
//   });
// });


var app = {
  inicio: function(){
    this.iniciaFastClick();
  },

  iniciaFastClick: function(){
    FastClick.attach(document.body);
  },

  dispositivoListo: function(){
    navigator.geolocation.getCurrentPosition(app.coordenadasMapa, app.errorAlSolicitarLocalizacion);
  },

  coordenadasMapa: function(position){
  // var miMapa = L.map('caminoseguro').setView([position.coords.latitude, position.coords.longitude], 13);
  var miMapa = L.map('caminoseguro', {
                fadeAnimation: false,
                zoomAnimation: false,
                markerZoomAnimation: false
              }).setView([40.453191, -3.509236], 13);
  L.titleLayer('https://api.mapbox.com/styles/v1/pedrocalas/cj9xf83rz6muv2sobidhajg36/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicGVkcm9jYWxhcyIsImEiOiJjajJ4cjluMTMwMThtMndxOGg4Y3NoZWkzIn0.83VnuZwLtidmCHxuP1briA',{
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
    maxZoom: 18,
    updateWhenIdle: true,
    reuseTiles: true }
  ).addTo(miMapa);

  var escuelalocation = [40.46, -3.5006939999999996];
	var escuela1 = L.marker(escuelalocation)
	escuela1.bindPopup('<p>Latitud:'+location[0]+'</p><p>Longitud:'+location[1]+'</p>');
	escuela1.addTo(miMapa);
  },

  var boundaries = new L.WFS({
   url: 'http://localhost:8080/geoserver/earth/ows?',
   typeNS: 'earth',
   typeName: 'countries',
   crs: L.CRS.EPSG4326,
   style: {
      'color': "#e12a2a",
      'fill' : false,
      'dashArray': "5, 5",
      'weight': 2,
      'opacity': 0.6
    }
  }, new L.Format.GeoJSON({crs: L.CRS.EPSG4326})).addTo(map);

  errorAlSolicitarLocalizacion: function(error){
    console.log(error.code + ': ' + error.message);
  }

};

  if('addEventListener' in document){
  document.addEventListener('DOMContentLoaded', function(){
    app.inicio();
  }, false);
  document.addEventListener('deviceready', function(){
    app.dispositivoListo();
  }, false);
}
